<?php
//открываем сессию
session_start();
// переменная, в которую будем сохранять результат работы
$data['result']='error';

// если данные были отправлены методом POST, то...
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // устанавливаем результат, равный success
    $data['result']='success';

//    получить тему
    if (isset($_POST['subject'])) {
        $subject = $_POST['subject'];
        $subject = preg_replace("/  +/", " ", $subject);
        $subject = strip_tags($subject);
        $subject = htmlspecialchars($subject);
        $data['subject'] = $subject;
    }
    else{
        $data['result']='error';
    }

//    получить имя
    if (isset($_POST['name'])) {
        $name = $_POST['name'];
        $name = preg_replace("/  +/", " ", $name);
        $name = strip_tags($name);
        $name = htmlspecialchars($name);
        $data['name'] = $name;
    }
    else{
        $data['result']='error';
    }

//    получить email
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
        if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
            $data['result'] = 'error';
            $data['email'] = 'error';
        }
        else{
            $data['email'] = $email;
        }
    }

//    получить сообщение
    if (isset($_POST['message'])) {

        $message = $_POST['message'];
        if($message === ''){
            $data['message'] = 'Нет сообщения';
        }
        else{
            $message = preg_replace("/  +/", " ", $message);
            $message = strip_tags($message);
            $message = htmlspecialchars($message);
            $data['message'] = $message;
        }
    }

//    получить услугу
    if (isset($_POST['service'])) {
        $service = $_POST['service'];
        $service = preg_replace("/  +/", " ", $service);
        $service = strip_tags($service);
        $service = htmlspecialchars($service);
        $data['service'] = $service;
    }
    else{
        $data['service'] = 'Нет услуги';
    }

}
else {
    //данные не были отправлены методом пост
    $data['result']='error';
}

// дальнейшие действия (ошибок не обнаружено)
if ($data['result']=='success') {

    $sendto   = "gpbro@mail.ru"; // почта, на которую будет приходить письмо

// Формирование заголовка письма
    $headers  = "From: " . "webmaster@gpbro.ru" . "\r\n";
    $headers .= "Reply-To: ". "" . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html;charset=utf-8 \r\n";

// Формирование тела письма
    $msg  = "<html><body style='font-family:Arial,sans-serif;'>";
    $msg .= "<h2 style='font-weight:bold; text-decoration: underline;'> ".$data['subject']."</h2>\r\n";
    $msg .= "<p><strong>Имя:&nbsp</strong> ".$data['name']."</p>\r\n";
    $msg .= "<p><strong>Email:&nbsp</strong> ".$data['email']."</p>\r\n";
    $msg .= "<p><strong>Услуга:&nbsp</strong> ".$data['service']."</p>\r\n";
    $msg .= "<p><strong>Сообщение:&nbsp</strong> ".$data['message']."</p>\r\n";
    $msg .= "</body></html>";

// отправка сообщения
    mail($sendto, $data['subject'], $msg, $headers);
}

// формируем ответ, который отправим клиенту
echo json_encode($data);
