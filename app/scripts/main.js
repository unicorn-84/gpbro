(function func($) {
  const $requestForm = $("#requestForm");
  const $contactForm = $("#contactForm");
  const $requestModal = $("#requestModal");
  const $successModal = $("#successModal");
  const $errorModal = $("#errorModal");
  const $requestEmail = $("#requestEmail");
  const $contactEmail = $("#contactEmail");

  if ("ontouchstart" in window || (window.DocumentTouch && document instanceof window.DocumentTouch) || navigator.maxTouchPoints > 0 || window.navigator.msMaxTouchPoints > 0) {
    document.documentElement.classList.remove("no-touch");
    document.documentElement.classList.add("touch");
  }
  $(window).on("scroll", () => {
    const y = $(window).scrollTop();
    const topMenu = $("#topMenu");
    const h = topMenu.height();
    if ((y > h + 30) && ($(window).outerWidth() > 768)) {
      topMenu.addClass("opaque");
    } else if (y < h + 30) {
      topMenu.removeClass("opaque");
    } else {
      topMenu.addClass("opaque");
    }
  });
  const toggleButton = $("#toggle-btn");
  const nav = $("ul#nav");

  toggleButton.on("click", (e) => {
    e.preventDefault();
    if (nav.is(":visible")) toggleButton.children("span").removeClass("glyphicon-remove").addClass("glyphicon-menu-hamburger");
    else toggleButton.children("span").removeClass("glyphicon-menu-hamburger").addClass("glyphicon-remove");
    nav.slideToggle("fast");
  });

  if (toggleButton.is(":visible")) nav.addClass("mobile");
  $(window).resize(() => {
    if (toggleButton.is(":visible")) {
      nav.addClass("mobile");
      nav.css("display", "none");
      toggleButton.children("span").removeClass("glyphicon-remove").addClass("glyphicon-menu-hamburger");
    } else {
      nav.removeClass("mobile");
      nav.css("display", "flex");
    }
  });

  $("ul#nav li a").on("click", () => {
    if (nav.hasClass("mobile")) nav.fadeOut("fast");
    if (nav.is(":visible")) toggleButton.children("span").removeClass("glyphicon-remove").addClass("glyphicon-menu-hamburger");
  });
  $(".smoothscroll").on("click", (e) => {
    e.preventDefault();
    const target = this.hash;
    const $target = $(target);

    $("html, body")
      .stop()
      .animate({ scrollTop: $target.offset().top }, 800, "swing", () => {
        window.location.hash = target;
      });
  });

  const navli = $(".nav li");
  const sections = $(".tracked");
  if ($("body").hasClass("homepage")) {
    sections.waypoint({
      handler(direction) {
        let activeSection = this.element.id;
        if (direction === "up") activeSection = this.element.previousElementSibling.id;
        const activeLink = $(`#nav a[href='#${activeSection}']`);
        navli.removeClass("current");
        activeLink.parent().addClass("current");
      },
      offset: "25%",
    });
  }
  const pxShow = 300; // height on which the button will show
  const fadeInTime = 400; // how slow/fast you want the button to show
  const fadeOutTime = 400; // how slow/fast you want the button to hide

  // Show or hide the sticky footer button
  jQuery(window).scroll(() => {
    if (jQuery(window).scrollTop() >= pxShow) {
      jQuery("#goTop").fadeIn(fadeInTime);
    } else {
      jQuery("#goTop").fadeOut(fadeOutTime);
    }
  });


  $("button").on("click", () => {
    this.blur();
  });

  $(".disable").on("click", (e) => {
    this.blur();
    e.preventDefault();
  });

  $requestModal.on("show.bs.modal", (event) => {
    const button = $(event.relatedTarget); // Button that triggered the modal
    const recipient = button.data("whatever");
    const modal = $(this);
    modal.find("#requestService").val(recipient);
  });
  if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
    $requestModal.on("show.bs.modal", () => {
      // Position modal absolute and bump it down to the scrollPosition
      $(this)
        .css({
          position: "absolute",
          marginTop: `${$(window).scrollTop()}px`,
          bottom: "auto",
        });
      setTimeout(() => {
        $(".modal-backdrop").css({
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: `${Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight)}px`,
        });
      }, 0);
    });
  }
  $requestForm.submit((event) => {
    event.preventDefault();

    if ($requestEmail.hasClass("error")) {
      $requestEmail.removeClass("error");
    }

    const reqData = {
      subject: "Запрос услуги",
      name: $("#requestName").val(),
      email: $requestEmail.val(),
      message: $("#requestMessage").val(),
      service: $("#requestService").val(),
    };

    $.ajax({
      type: "POST",
      url: "contact.php",
      data: reqData,
      dataType: "json",
      cache: false,
      success(respData) {
        if (respData.email === "error") {
          $requestEmail.addClass("error");
        } else {
          $requestModal.modal("hide");
          $requestModal.one("hidden.bs.modal", () => {
            $requestForm.trigger("reset");
            $successModal.modal("show");
          });
        }
      },
      error() {
        $requestModal.modal("hide");
        $requestModal.one("hidden.bs.modal", () => {
          $errorModal.modal("show");
        });
        return false;
      },
    });
  });

  $contactForm.submit((event) => {
    event.preventDefault();

    if ($contactEmail.hasClass("error")) {
      $contactEmail.removeClass("error");
    }

    const reqData = {
      subject: "Сообщение из формы обратной связи",
      name: $("#contactName").val(),
      email: $contactEmail.val(),
      message: $("#contactMessage").val(),
    };

    $.ajax({
      type: "POST",
      url: "contact.php",
      data: reqData,
      dataType: "json",
      cache: false,
      success(respData) {
        if (respData.email === "error") {
          $contactEmail.addClass("error");
        } else {
          $contactForm.trigger("reset");
          $successModal.modal("show");
        }
      },
      error() {
        $errorModal.modal("show");
      },
    });
    return false;
  });
}(jQuery));
